<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
    {!! Html::style( asset('assets/admin/bootstrap/css/bootstrap.min.css')) !!}
    <!-- Font Awesome -->
    {!! Html::style( asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')) !!}
    <!-- Ionicons -->
    {!! Html::style( asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')) !!}
     <!-- Theme style -->
    {!! Html::style( asset('assets/admin/dist/css/AdminLTE.min.css')) !!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {!! Html::style( asset('assets/admin/dist/css/skins/_all-skins.min.css')) !!}
    <!-- iCheck -->
    {!! Html::style( asset('assets/admin/plugins/iCheck/flat/blue.css')) !!}
    <!-- Morris chart -->
    {!! Html::style( asset('assets/admin/plugins/morris/morris.css')) !!}
    <!-- jvectormap -->
    {!! Html::style( asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')) !!}
    <!-- Date Picker -->
    {!! Html::style( asset('assets/admin/plugins/datepicker/datepicker3.css')) !!}
    <!-- Daterange picker -->
    {!! Html::style( asset('assets/admin/plugins/daterangepicker/daterangepicker-bs3.css')) !!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!! Html::style( asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')) !!}


 @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    @yield('content')

	<!-- jQuery 2.1.4 -->
	{!! Html::script( asset('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js') ) !!}

    <!-- Bootstrap 3.3.5 -->
    {!! Html::script( asset('assets/admin/bootstrap/js/bootstrap.min.js') ) !!}
    
    <!-- Slimscroll -->
    {!! Html::script( asset('assets/admin/plugins/slimScroll/jquery.slimscroll.min.js') ) !!}
    <!-- FastClick -->
    {!! Html::script( asset('assets/admin/plugins/fastclick/fastclick.min.js') ) !!}
    <!-- AdminLTE App -->
    {!! Html::script( asset('assets/admin/dist/js/app.min.js') ) !!}
    <!-- AdminLTE for demo purposes -->
    {!! Html::script( asset('assets/admin/dist/js/demo.js') ) !!}

    {!! Html::script( asset('assets/admin/js/delete.js') ) !!}

    {!! Html::script( asset('assets/admin/js/property.js') ) !!}

   
	@yield('scripts')
</body>
</html>
