<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contracts\UserInterface;

use App\Http\Requests;

class MobileController extends Controller
{
    public function getStatistic()
    {
    	$users = [];
    	$data = [
    		'users' => $users
    	];
    	return view('statistic', $data);
    }

    public function getBestAbonents(UserInterface $userRepo)
	{
		$users = $userRepo->getAll();
		$count = count($users);
		$price = 0;
		$internetArray = [];
		$mobileArray = [];
		foreach($users as $user)
		{
			$price+=$user->price;
		}
		$middlePrice = $price/$count;
		$bestAbonents = $userRepo->getBestAbonents($middlePrice);
		$data = [
			'users' => $bestAbonents
		];
		return view('statistic', $data);
	}

	public function getBestMobileAbonents(UserInterface $userRepo)
	{
		$users = $userRepo->getAll();
		$count = count($users);
		$price = 0;
		foreach($users as $user)
		{
			$price+=$user->mobile_price;
		}
		$middlePrice = $price/$count;
		$bestMobileAbonents = $userRepo->getBestMobileAbonents($middlePrice);
		$data = [
			'users' => $bestMobileAbonents
		];
		return view('statistic', $data);
	}

	public function getBestInternetAbonents(UserInterface $userRepo)
	{
		$users = $userRepo->getAll();
		$count = count($users);
		$price = 0;
		foreach($users as $user)
		{
			$price+=$user->internet_price;
		}
		$middlePrice = $price/$count;
		$bestInternetAbonents = $userRepo->getBestInternetAbonents($middlePrice);
		$data = [
			'users' => $bestInternetAbonents
		];
		return view('statistic', $data);
	}

	public function getBestMobileAbonentsByPercent(UserInterface $userRepo)
	{
		$users = $userRepo->getBestMobileAbonentsByPercent();
		$data = [
			'users' => $users
		];
		return view('statistic', $data);
	}

	public function getBestInternetAbonentsByPercent(UserInterface $userRepo)
	{
		$users = $userRepo->getBestInternetAbonentsByPercent();
		$data = [
			'users' => $users
		];
		return view('statistic', $data);
	}
}