<?php namespace App\Service;
	
use App\Contracts\UserInterface;
use App\User;

class UserService implements UserInterface
{
	/**
     * Create a new instance of PhotoService.
     *
     * @return void
     */
	function __construct()
	{
		$this->user = new User();
	}

	/**
     * get photo
     *
     * @param integer $id
     * @return photo
     */
	public function getOne($id)
	{
		return $this->user->find($id);
	}

    /**
     * all photo
     *
     * @param
     * @return photo
     */
    public function getAll()

    {
        return $this->user->get();
    }

	/**
     * update photo
     *
     * @param integer $id
     * @param integer $param
     * @return bool
     */
	public function updateUser($id, $param)
    { 
        return $this->getOne($id)->update($param);
    }

	/**
	  * create new photo 
	  *
	  * @param $params
	  * @return photo 
	  */ 
	public function add($params)
	{
        $user = $this->user->create($params);
        return $user;
	}

    /**
     * Delete photo.
     *
     * @return Collection
     */
    public function deleteOne($id)
    {
    	$user = $this->user->find($id)->delete();
    	return $user;
    }

    public function getBestAbonents($middlePrice)
    {
        return $this->user->where('price', '>=', $middlePrice)->orderBy('price', 'desc')->take(20)->skip(20)->get();
    }

    public function getBestMobileAbonents($middlePrice)
    {

        return $this->user->where('mobile_price', '>=', $middlePrice)->orderBy('mobile_price', 'desc')->take(20)->skip(20)->get();
    }

    public function getBestInternetAbonents($middlePrice)
    {
        return $this->user->where('internet_price', '>=', $middlePrice)->orderBy('internet_price', 'desc')->take(20)->skip(20)->get();
    }

    public function getBestMobileAbonentsByPercent()
    {
        return $this->user->where('mobile_percent', '>=', '50')->orderBy('mobile_percent', 'desc')->take(20)->skip(20)->get();
    }
    public function getBestInternetAbonentsByPercent()
    {
        return $this->user->where('internet_percent', '>=', '50')->orderBy('internet_percent', 'desc')->take(20)->skip(20)->get();
    }
}