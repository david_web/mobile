$(document).ready(function(){
	window.propertyNumber = '';
	$('#propertynumberofmortgages').change(function(){
		propertyNumber = $(this).val();
		if((propertyNumber == '') || (propertyNumber == 0)){
			$('.first_mortgage').hide();
			$('.second_mortgage').hide();
			$('.third_mortgage').hide();

		}else if(propertyNumber == 1){
			$('.first_mortgage').show();
			$('.second_mortgage').hide();
			$('.third_mortgage').hide();

		}else if(propertyNumber == 2){
			$('.first_mortgage').show();
			$('.second_mortgage').show();
			$('.third_mortgage').hide();
		}else{
			$('.first_mortgage').show();
			$('.second_mortgage').show();
			$('.third_mortgage').show();
		}
	})
});