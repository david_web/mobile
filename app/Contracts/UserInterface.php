<?php namespace App\Contracts;
	
interface UserInterface
{
	/**
    * get photo
    *
    * @param integer $id
    * @return photo
    */
	public function getOne($id);

    /**
    * Get all photos.
    *
    * @return Collection
    */
    public function getAll();

	/**
    * update photo
    *
    * @param integer $id
    * @param integer $param
    * @return bool
    */
	public function updateUser($id,$param);

	/**
    * create new photo 
    *
    * @param $params
    * @return photo 
    */ 
	public function add($params);

    /**
    * Delete photo.
    *
    * @param $id
    * @return Collection
    */
    public function deleteOne($id);

    public function getBestAbonents($middlePrice);

    public function getBestMobileAbonents($middlePrice);

    public function getBestInternetAbonents($middlePrice);

    public function getBestMobileAbonentsByPercent();

    public function getBestInternetAbonentsByPercent();
    
}