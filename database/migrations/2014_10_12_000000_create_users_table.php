<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mobile_number')->unique();
            $table->integer('price');
            $table->float('internet_percent');
            $table->float('mobile_percent');
            $table->float('internet_price');
            $table->float('mobile_price');
            $table->float('local_calls_percent');
            $table->float('abroad_calls_percent');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
