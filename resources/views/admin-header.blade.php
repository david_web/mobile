<header class="main-header">
				<!-- Logo -->
				<a href="#" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>ST</b>UP</span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>ADMIN</b></span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<span class="hidden-xs">Admin</span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-right">
											<a href="#" class="btn btn-default btn-flat">Log out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- sidebar menu: : style can be found in sidebar.less -->
						<ul class="sidebar-menu">
		                    <li class="treeview">
								<a href="#">
									<span>Best abonents</span>
								</a>
								<ul class="treeview-menu">
									<li><a href="{{ action('MobileController@getBestAbonents') }}">Best abonents</a></li>
			                        <li><a href="{{ action('MobileController@getBestMobileAbonents') }}">Best mobile abonents</a></li>
			                        <li><a href="{{ action('MobileController@getBestInternetAbonents') }}">Best internet abonents</a></li>
			                    </ul>
							</li>
		                    <li class="treeview">
								<a href="#">
									<span>Best abonents(according percent)</span>
								</a>
								<ul class="treeview-menu">
			                        <li><a href="{{ action('MobileController@getBestMobileAbonentsByPercent') }}">Best mobile abonents</a></li>
			                        <li><a href="{{ action('MobileController@getBestInternetAbonentsByPercent') }}">Best internet abonents</a></li>
			                    </ul>
							</li>
						</ul>
				</section>
		<!-- /.sidebar -->
		</aside>