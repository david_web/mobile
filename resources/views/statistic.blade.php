@extends('app-admin')
@section('content')
<div class="wrapper"> 
    @include('admin-header') 
    <div class="content-wrapper">
		<!-- <section class="content-header">
	            <h1>
	                Table
	            </h1>
    	</section> -->
	    <section class="content">
			<div class="row">
				@if(count($users) > 0)
					<div class="container">
					    <h2>Table</h2>                                                                                
				        <div class="table-responsive">          
				            <table class="table">
				    			<thead>
				      				<tr>
								        <th>Mobile number</th>
								        <th>Monthly expenses</th>
								        <th>Mobile</th>
								        <th>Internet</th>
								    </tr>
								</thead>
								@foreach($users as $key => $user)
			    				<tbody>
								        <tr>
									        <td>{{$user->mobile_number}}</td>
									        <td>{{$user->price}}</td>
									        <td>{{$user->mobile_price}}</td>
									        <td>{{$user->internet_price}}</td>
								        </tr>
							    </tbody>
							    @endforeach
						    </table>
						</div>
				    </div>
				@endif
			</div>
		</section>
	</div>
	@include('admin-footer')
</div>
@endsection