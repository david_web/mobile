<?php

use Illuminate\Database\Seeder;
use App\User;

class MobileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 1000; $i++ )
        {
        	$mobile = mt_rand(10000000, 99999999);
        	$price = mt_rand(0,30000);
        	$internet_percent = mt_rand(0,100);
        	$mobile_percent = 100 - $internet_percent;
            $internet_price = $price*$internet_percent/100;
            $mobile_price = $price*$mobile_percent/100;
        	$local_calls = mt_rand(0,100);
        	$abroad_calls = 100 - $local_calls;
        	User::create(array(
				'mobile_number' => $mobile,
				'price' => $price,
				'internet_percent' => $internet_percent,
				'mobile_percent' => $mobile_percent,
                'internet_price' => $internet_price,
                'mobile_price' => $mobile_price,
				'local_calls_percent' => $local_calls,
				'abroad_calls_percent' => $abroad_calls
			));
        }
    }
}
